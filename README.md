Some experiments with [Eve](http://witheve.com/)

---

[2048 Game](http://play.witheve.com/#gist:b01c381fe930561b8912b06415fa8cac-2048.eve.md) ([source code](code/2048.eve.md))

![2048 screenshot](images/2048_screenshot.png)

---